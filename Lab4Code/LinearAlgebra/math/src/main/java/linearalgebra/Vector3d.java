//2042714
//Sungeun Kim

package linearalgebra;

public class Vector3d {
    public static void main(String[] args) {
        //Examples on the instruction
        Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);

        //Print Vector1 and Vector2
        System.out.println("Vector 1: (" + vector1.getX() + ", " + vector1.getY() + ", " + vector1.getZ() + ")");
        System.out.println("Vector 2: (" + vector2.getX() + ", " + vector2.getY() + ", " + vector2.getZ() + ")");
        //Print the three cases
        System.out.println("Magnitude of Vector1: " + vector1.magnitude());
        System.out.println("Magnitude of Vector2: " + vector2.magnitude());
        System.out.println("Dot Product: " + vector1.dotProduct(vector2));
        Vector3d sumTwoVectors = vector1.add(vector2);
        System.out.println("Sum: (" + sumTwoVectors.getX() + ", " + sumTwoVectors.getY() + ", " + sumTwoVectors.getZ() + ")");
    }
    //Declare three fields
    double x;
    double y;
    double z;

    //Constructor
    public Vector3d (double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Three getter methods
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }

    public double magnitude() {
        return Math.sqrt((x * x) + (y * y) + (z * z));
    }
    public double dotProduct(Vector3d anotherVector3d) {
        return (x * anotherVector3d.getX()) + (y * anotherVector3d.getY()) + (z * anotherVector3d.getZ());
    }
    public Vector3d add(Vector3d anotherVector3d) {
        double newXValue = this.x + anotherVector3d.getX();
        double newYValue = this.y + anotherVector3d.getY();
        double newZValue = this.z + anotherVector3d.getZ();
        return new Vector3d(newXValue, newYValue, newZValue);
    }
}

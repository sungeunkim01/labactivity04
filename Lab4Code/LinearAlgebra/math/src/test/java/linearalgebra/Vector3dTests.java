//Sungeun Kim
//2042714

package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class Vector3dTests {
    
    @Test
    public void testGetterMethod() {
        Vector3d expectedVector = new Vector3d(1.0, 1.0, 2.0);
        Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
        assertEquals(expectedVector.getX(), vector1.getX(), 0.001);
        assertEquals(expectedVector.getY(), vector1.getY(), 0.001);
        assertEquals(expectedVector.getZ(), vector1.getZ(), 0.001);
    };

    @Test
    public void testMagnitude() {
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
        double magnitude = vector2.magnitude();
        assertEquals(5.385, magnitude, 0.001);
    }

    @Test
    public void testDotProduct() {
        Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
        double dotProduct = vector1.dotProduct(vector2);
        assertEquals(13.0, dotProduct, 0.001);
    }

    @Test
    public void testAdd() {
        Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
        Vector3d expectedVector = new Vector3d(3.0, 4.0, 6.0);
        Vector3d sumVector = vector1.add(vector2);
        
        assertEquals(expectedVector.getX(), sumVector.getX(), 0.001);
        assertEquals(expectedVector.getY(), sumVector.getY(), 0.001);
        assertEquals(expectedVector.getZ(), sumVector.getZ(), 0.001);
    }
}
